# 懒虫之家

   懒虫之家欢迎您！
   
   懒虫系列软件如下，如有需要，敬请下载。
   
   
## 懒虫分身   [点我下载](https://gitlab.com/zc6402856/LazyHome/blob/master/release/Avatar_V1.0.apk)
   
   软件分身多开。 
 
 
## 懒虫理证   [点我下载](https://gitlab.com/zc6402856/LazyHome/blob/master/release/Certificate_V1.0.apk)
    
    管理身份证件，银行卡，电话对应关系。
    
    
## 懒虫网赚   [点我下载](https://gitlab.com/zc6402856/LazyHome/blob/master/release/Earn_V1.0.apk)
   
   网赚资源分享。

    
## 懒虫乐园   [点我下载](https://gitlab.com/zc6402856/LazyHome/blob/master/release/Recreation_V1.0.apk)
   
   视频资源分享。

 
## 懒虫传信   [点我下载](https://gitlab.com/zc6402856/LazyHome/blob/master/release/SMSForward_V1.0.apk)
   
   短信自动转发到邮箱，微信，短信。
 
 
## 懒虫送信   [点我下载](https://gitlab.com/zc6402856/LazyHome/blob/master/release/SMSSend_V1.0.apk)
   
   定时发送短信，达到多电话卡签到。
 
 
## 懒虫宝镖   [点我下载](https://gitlab.com/zc6402856/LazyHome/blob/master/release/Safe_V1.0.apk)
   
   限制电话，短信。控制儿童使用手机。


## 懒虫小秘   [点我下载](https://gitlab.com/zc6402856/LazyHome/blob/master/release/Secretary_V1.0.apk)
   
   语音指挥你的秘书发短信，打电话，虚拟来电。和你的小秘聊天。
 
 
## 懒虫育宝   [点我下载](https://gitlab.com/zc6402856/LazyHome/blob/master/release/Teach_V1.0.apk)
   
   儿童教育资源分享。


